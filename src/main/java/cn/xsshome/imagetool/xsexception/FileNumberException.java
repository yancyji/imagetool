package cn.xsshome.imagetool.xsexception;

/**
 * Description 自定义异常
 * ProjectName imagetool
 * Created by 小帅丶 on 2022-04-18
 * Version 1.0
 */

public class FileNumberException extends Throwable {
    public FileNumberException() {
    }

    public FileNumberException(String message) {
        super(message);
    }
}
